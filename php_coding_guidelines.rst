======================================
PHP Coding Guidelines & Best Practices
======================================

General considerations
----------------------

* Follow the PSR-2 standard for code formatting
* Almost every PHP file in Flow contains exactly one class and does not output anything
  if it is called directly. Therefore you start your file with a ``<?php`` tag and must not end it
  with the closing ``?>``.
* Every file must contain a header stating namespace and licensing information

  * Declare your namespace.
  * The copyright header itself must not start with ``/**``, as this may confuse
    documentation generators!
