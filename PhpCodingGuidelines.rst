=====================
PHP Coding Guidelines
=====================

This document defines technical and style guidelines on how to code in PHP when doing Appetiser projects.

General considerations
----------------------

* Follow the `PSR-2`_ standard for code formatting
* PHP files must contains exactly one class and does not output anything
  if it is called directly. Therefore you start your file with a ``<?php`` tag and must NOT end it
  with the closing ``?>``.
* Every file must contain a header stating namespace
* Most web and API projects of Appetiser are based on `Laravel`_
* PhpStorm will be the official IDE

Namespace, Interface and Class names
------------------------------------

* Only the characters a-z, A-Z and 0-9 are allowed for namespace and class names.
* Namespaces are usually written in ``UpperCamelCase`` but variations are allowed for well
  established names and abbreviations.
* Class names are always written in ``UpperCamelCase``.
* The main purpose of namespaces is categorization and ordering
* Class names must be nouns, never adjectives.

Method names
------------

All method names are written in lowerCamelCase. In order to avoid problems with different
filesystems, only the characters a-z, A-Z and 0-9 are allowed for method names – don't use
special characters.

Make method names descriptive, but keep them concise at the same time. Constructors must
always be called ``__construct()``, never use the class name as a method
name.

* ``myMethod()``
* ``someCoolMethodName()``
* ``betterWriteLongMethodNamesThanNamesNobodyUnderstands()``
* ``singLoudly()``
* ``__construct()``


Variable names
--------------

In the PHP world, there are lots of argument when it comes to variable naming conventions.
Some uses a ``lowerCamelCase`` convention while others use ``snake_case`` but basically there is no clear rule on what to use.
In Appetiser, we wll use all lowercase ``snake_case`` as the convention since variables in PHP are case sensitive thus
an all lowercase ``snake_case`` convention is easier to use, prevents case errors and can be easily distinguishable from
method and function names.

Variable names should be:
* self-explanatory
* not shortened beyond recognition, but rather longer if it makes their meaning clearer

The following example shows two variables with the same meaning but different naming.

*Correct naming of variables*

* ``$singleton_objects``
* ``$arguments_array``
* ``$turn_off_the_app``

*Incorrect naming of variables*

* ``$sObjRgstry``
* ``$arg_Arr``
* ``$cxny``
* ``$a`` ``$b`` ``$c``

*If variables are expected to contain boolean data it must start with ``is`` , ``has`` or ``can``*

* ``$is_admin``
* ``$can_delete``
* ``$has_payment``

As a special exception you may use variable names like ``$i``, ``$j`` and ``$k`` for
numeric indexes in ``for`` loops if it's clear what they mean on the first sight. But even
then you should want to avoid them.

Filenames
---------

These are the rules for naming files:

* All filenames are ``UpperCamelCase``.
* Class and interface files are named according to the class or interface they represent
* Each file must contain only one class or interface
* Names of files containing code for unit tests must be the same as the class which is
  tested, appended with "Test.php".
* Files are placed in a directory structure representing the namespace structure.


Constant names
--------------

All constant names are written in ``UPPERCASE``. This includes ``TRUE``, ``FALSE`` and
``NULL``. Words can be separated by underscores - you can also use the underscore to group
constants thematically:

* ``STUFF_LEVEL``
* ``COOLNESS_FACTOR``
* ``PATTERN_MATCH_EMAILADDRESS``
* ``PATTERN_MATCH_VALIDHTMLTAGS``

Control Structures
------------------

The structure keywords such as if, for, foreach, while, switch should be followed by a space as should parameter/argument lists and values.
Braces should be placed on a same line and break should have the same tab as its case.

Usage::

    if ($arg === true){
        //do something here
    }elseif ($arg === null){
        //do something else here
    }else{
        //catch all do something here
    }

    foreach ($array as $key => $value){
        //loop here
    }

    for ($i = 0; $i < $max; $i++){
        //loop here
    }



.. _laravel-guide:
.. toctree::
   :maxdepth: 2
   :caption:Laravel Guidelines

   ecosystem
   controllers
   models


.. _PSR-2: https://www.php-fig.org/psr/psr-2/
.. _Laravel: https://www.laravel.com/