Appetiser's Web Development Standards
=====================================

It is important for the Web Developers of Appetiser to have a common way of doing things when they work on projects.
Coding Standards will address the issues of developers having different styles and approach and are an important
factor in achieving high quality of code.

.. toctree::
	:titlesonly:

	PhpCodingGuidelines
	LaravelGuidelines/index
	JsCodingGuidelines
	FrontendGuidelines
	ApiCodingGuidelines