==================
Laravel Guidelines
==================

Getting Started
---------------

All Laravel projects needs to be saved on a Git Repo. Current repository used is Gitlab.
To get started with a new Laravel project you need to:

* Clone the repo of an existing project or if the project is new use the latest current stable version.
* Run ``composer install``. Do not run ``composer update`` for projects that are already in the middle of devlopment as it might conflict the composer lock file
* Migrate database by calling ``php artisan migrate``
* Setup the .env file



Controllers
-----------

* Same as in the PHP Guidlines, all method names are written in lowerCamelCase. In order to avoid problems with different filesystems, only the characters a-z, A-Z and 0-9 are allowed for method names – don't use special characters.
* Only controllers related to the Frontend ( public pages ) must be on document root of the Controllers directory
* All other controllers must be organized under sub-folders for example /Admin , /Api, /Ajax etc...

Models
------

* Projects must have a Models directory found under /app folder of a project root directory.
An /app/Models folder should be created on all projects when there is none.

Model filenames should be the name of the table in UpperCamelCase.

Table and field naming
----------------------

* Table and field names MUST be lowercase and use Snake Case ( snake_case ).
* Table names should use the plural form of the actual real life object it is storing. Like for example, the table name for blog posts should be posts not post.
* Primary keys should be named ``table_name_in_singular_form_id`` in the table names. For example table Posts should have a primary key ``post_id``. For tables
that are considered pivot tables, a primary key of ``pivot_id`` or ``map_id`` may be acceptable

Adding tables, index or inserting sample data
---------------------------------------------

* All tables must be created using database migration by calling the ``php artisan make:migration create_table_name --create=table_name`` command. Do not use PhpMyAdmin, the console or any other database IDE to create tables.
*


Routes
------

WIP

Blade and Views
---------------

WIP

Unit Testing
------------

WIP

